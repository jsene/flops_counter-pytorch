import argparse
import time 
import torchvision.models as models
import torch.nn as nn
import torch
from flops_counter import add_flops_counting_methods
from spectralnet import SpectralNet
from dwsnet import DWSNet

def get_network(num_bands, net=0):
    if net == 0:
        net = SpectralNet(num_bands=num_bands)
    if net == 1:
        net = DWSNet(num_bands=num_bands)
    tm = time.strftime("%Y-%m-%d_%H:%M:%S", time.gmtime())
    file_name = 'SpectralNet-'+str(num_bands)+tm 
    return net, file_name 

def flops_to_string(flops):
    if flops // 10**9 > 0:
        return str(round(flops / 10.**9, 2)) + 'GMac'
    elif flops // 10**6 > 0:
        return str(round(flops / 10.**6, 2)) + 'MMac'
    elif flops // 10**3 > 0:
        return str(round(flops / 10.**3, 2)) + 'KMac'
    return str(flops) + 'Mac'

def get_model_parameters_number(model, as_string=True):
    params_num = sum(p.numel() for p in model.parameters() if p.requires_grad)
    if not as_string:
        return params_num

    if params_num // 10 ** 6 > 0:
        return str(round(params_num / 10 ** 6, 2)) + 'M'
    elif params_num // 10 ** 3:
        return str(round(params_num / 10 ** 3, 2)) + 'k'

    return str(params_num)

pt_models = { 'resnet18': models.resnet18, 'resnet50': models.resnet50,
              'alexnet': models.alexnet,
              'vgg16': models.vgg16, 'squeezenet': models.squeezenet1_0,
              'densenet': models.densenet161}

if __name__ == '__main__':
    net, f = get_network(3, net=0)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    with torch.cuda.device(0):
        batch = torch.FloatTensor(1, 3, 64, 64)
        model = add_flops_counting_methods(net)
        model.eval().start_flops_count()
        _ = model(batch)

        #print(model)
        print('Flops:  {}'.format(flops_to_string(model.compute_average_flops_cost())))
        print('Params: ' + get_model_parameters_number(model))
